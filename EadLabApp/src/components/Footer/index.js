import React from 'react';
import { StyleSheet, View, TouchableOpacity, Platform } from 'react-native';
import { Entypo, Feather, Ionicons } from '@expo/vector-icons';

// gambiarra só pra agora mesmo
var iconOpacity = 0.3;

export default function Footer({ navigate }) {
  return (
    <View style={styles.container}>
      <View style={styles.iconsContainer}>
        <TouchableOpacity onPress={() => navigate('Menu')}>
          <Entypo name="game-controller" size={35} color="white" style={styles.icons} />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigate('Ranking')}>
          <Ionicons name="ios-trophy" size={35} color="orange" style={styles.icons} />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigate('Perfil')}>
          <Feather name="user" size={35} color="white" style={styles.icons} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    height: 94,
    width: '100%',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4B79AB',
  },

  iconsContainer: {
    flexDirection: 'row',
    width: 342,
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  icons: {
    opacity: iconOpacity,
  },
});
