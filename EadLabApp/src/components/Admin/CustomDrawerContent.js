import * as React from 'react';
import { StyleSheet } from 'react-native';
import { DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { Feather } from '@expo/vector-icons';

const MenuDrawer = () => <Feather name="menu" size={35} color="#3F5E7C" />;

export default function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props} style={styles.drawerContent}>
      <DrawerItem
        label=""
        icon={() => <MenuDrawer />}
        onPress={() => props.navigation.closeDrawer()}
        style={styles.drawerItem}
      ></DrawerItem>
      <DrawerItemList {...props} activeBackgroundColor="#7FB6D6" labelStyle={styles.label} />
    </DrawerContentScrollView>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    backgroundColor: '#fff',
    paddingVertical: 5,
  },
  drawerItem: {
    marginLeft: 10,
  },
  label: {
    color: '#2A3859',
  },
});
