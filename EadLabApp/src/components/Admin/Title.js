import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Title({ viewTitle }) {
  return (
    <View style={styles.container}>
      <Text style={styles.viewTitle}>{viewTitle}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginBottom: 5,
  },

  viewTitle: {
    color: '#fff',
    fontSize: 30,
    textAlign: 'center',
    fontWeight: '500',
  },
});
