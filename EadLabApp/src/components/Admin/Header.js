import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Avatar,
  MenuItem,
  OverflowMenu,
  TopNavigation,
  TopNavigationAction,
} from '@ui-kitten/components';
import { MaterialIcons, Feather, FontAwesome } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const MenuDrawer = () => <Feather name="menu" size={35} color="#3F5E7C" />;
const MenuIcon = () => <Feather name="more-vertical" size={30} color="#3F5E7C" />;
const UserIcon = () => <FontAwesome name="user-circle-o" size={24} color="#3F5E7C" />;
const InfoIcon = () => <MaterialIcons name="info" size={25} color="#3F5E7C" />;
const LogoutIcon = () => <Feather name="log-out" size={24} color="#3F5E7C" />;

export default function AdminHeader({ navigation }) {
  const [menuVisible, setMenuVisible] = React.useState(false);

  const toggleMenu = () => {
    setMenuVisible(!menuVisible);
  };

  const renderMenuAction = () => <TopNavigationAction icon={MenuIcon} onPress={toggleMenu} />;

  const renderOverflowMenuAction = () => (
    <React.Fragment>
      <OverflowMenu
        anchor={renderMenuAction}
        visible={menuVisible}
        onBackdropPress={toggleMenu}
        style={styles.menuMore}
      >
        <MenuItem accessoryLeft={UserIcon} title="Administração" style={styles.menuMoreItem} />
        <MenuItem accessoryLeft={InfoIcon} title="Sobre" style={styles.menuMoreItem} />
        <MenuItem accessoryLeft={LogoutIcon} title="Deslogar" style={styles.menuMoreItem} />
      </OverflowMenu>
    </React.Fragment>
  );

  const renderTitle = () => (
    <View style={styles.titleContainer}>
      <Avatar style={styles.logo} source={require('../../assets/ead_lab_logo.png')} />
    </View>
  );

  const renderMenu = () => (
    <TouchableOpacity onPress={() => navigation.openDrawer()}>
      <View style={styles.titleContainer}>
        <MenuDrawer />
      </View>
    </TouchableOpacity>
  );

  return (
    <TopNavigation
      alignment="center"
      title={renderTitle}
      accessoryRight={renderOverflowMenuAction}
      accessoryLeft={renderMenu}
      style={styles.topNav}
    />
  );
}

const styles = StyleSheet.create({
  topNav: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    paddingVertical: 20,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
    shadowRadius: 8,

    elevation: 4,
  },
  titleContainer: {
    marginLeft: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuMore: {
    marginTop: 18,
    marginRight: 10,
  },
  logo: {
    height: 45,
    width: 45,
  },
});
