import React, { useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default function QuizCard() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>RESULTADOS</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(2,3,13,1)',
  },
  text: {
    color: 'white',
  },
});
