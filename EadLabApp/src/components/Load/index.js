import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function Load() {
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.containerLoad}>
        <ActivityIndicator size="large" color="white" />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  containerLoad: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
