import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { GameContext } from '../../contexts/game';
import { ScoreContext } from '../../contexts/score';
import { TimerContext } from '../../contexts/timer';

export default function QuizCard({ data, index }) {
  const { askIndex, setAskIndex } = useContext(GameContext);
  const { addPoints } = useContext(ScoreContext);
  const { resetTime } = useContext(TimerContext);
  const [answerColor, setAnswerColor] = useState({
    0: 'rgba(28,57,89,0.4)',
    1: 'rgba(28,57,89,0.4)',
    2: 'rgba(28,57,89,0.4)',
    3: 'rgba(28,57,89,0.4)',
  });

  function findCorrect() {
    for (var i = 0; i < 5; i++) {
      let quest = 'alternative_' + i;
      if (data[quest] === data.answer) return i;
    }
  }
  function checkAnswer(choice, index) {
    if (choice === data.answer) {
      addPoints();
      setAnswerColor((old) => {
        return { ...old, [index]: 'rgba(23,112,17,0.6)' };
      });
    } else {
      let correctAnswer = findCorrect();
      console.log(correctAnswer);
      setAnswerColor((old) => {
        return { ...old, [index]: 'rgba(255,11,11,0.5)', [correctAnswer]: 'rgba(23,112,17,0.6)' };
      });
    }
    setTimeout(() => {
      resetTime();
      setAskIndex(askIndex + 1);
    }, 1000);
  }

  useEffect(() => {
    setAnswerColor({
      0: 'rgba(28,57,89,0.4)',
      1: 'rgba(28,57,89,0.4)',
      2: 'rgba(28,57,89,0.4)',
      3: 'rgba(28,57,89,0.4)',
    });
  }, [data]);

  return (
    <View style={styles.container}>
      <Text style={styles.question}>{`${index + 1} - ${data.question}`}</Text>

      <View style={styles.responseList}>
        <TouchableOpacity
          style={[{ backgroundColor: answerColor[0] }, styles.responseItem]}
          onPress={() => checkAnswer(data.alternative_0, 0)}
        >
          <Text style={styles.text}>{`A - ${data.alternative_0}`}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[{ backgroundColor: answerColor[1] }, styles.responseItem]}
          onPress={() => checkAnswer(data.alternative_1, 1)}
        >
          <Text style={styles.text}>{`B - ${data.alternative_1}`}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[{ backgroundColor: answerColor[2] }, styles.responseItem]}
          onPress={() => checkAnswer(data.alternative_2, 2)}
        >
          <Text style={styles.text}>{`C - ${data.alternative_2}`}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[{ backgroundColor: answerColor[3] }, styles.responseItem]}
          onPress={() => checkAnswer(data.alternative_3, 3)}
        >
          <Text style={styles.text}>{`D - ${data.alternative_3}`}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    maxWidth: 400,
    borderRadius: 10,
    height: '60%',
    width: '80%',
    backgroundColor: 'rgba(56,77,117,0.6)',
    shadowColor: '#000',
    shadowOffset: { width: 6, height: 6 },
    shadowOpacity: 0.3,
  },
  question: {
    marginTop: 20,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 10,
    opacity: 0.8,
  },
  responseList: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    marginBottom: 10,
    marginTop: 10,
  },
  responseItem: {
    height: '15%',
    width: '90%',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.3,
  },
  text: {
    opacity: 0.9,
    color: 'white',
    fontSize: 16,
    fontWeight: '800',
    textAlign: 'center',
  },
});
