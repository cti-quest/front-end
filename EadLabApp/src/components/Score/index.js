import React, { useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ScoreContext } from '../../contexts/score';

export default function QuizCard() {
  const { score } = useContext(ScoreContext);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>PONTOS</Text>
      <View style={styles.body}>
        <Text style={styles.font}>{`${score}`}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  body: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height: 50,
    width: 110,
    backgroundColor: 'rgba(56,77,117,0.6)',
  },
  font: { fontSize: 35, color: 'white', opacity: 0.6 },
  title: { fontSize: 20, color: 'white', opacity: 0.6 },
});
