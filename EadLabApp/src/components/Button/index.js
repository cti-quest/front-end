import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function Button({ navigation, goTo, text }) {
  return (
    <View style={styles.buttonContainer}>
      <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate(goTo)}>
        <View style={styles.buttonCard}>
          <Text style={styles.buttonText}>{text}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginVertical: 40,
  },

  buttonCard: {
    height: 50,
    backgroundColor: '#252A45',
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 30,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.4,
    shadowRadius: 5,

    elevation: 4,
  },

  buttonText: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
  },
});
