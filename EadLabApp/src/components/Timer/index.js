import React, { useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { TimerContext } from '../../contexts/timer';

export default function QuizCard() {
  const { time } = useContext(TimerContext);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>TEMPO</Text>
      <View style={styles.body}>
        <Text style={styles.font}>
          {`${time.minutes < 10 ? '0' + time.minutes : time.minutes}:${
            time.seconds < 10 ? '0' + time.seconds : time.seconds
          }:${time.thousandth < 10 ? '0' + time.thousandth : time.thousandth}`}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  body: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height: 50,
    width: 140,
    backgroundColor: 'rgba(56,77,117,0.6)',
  },
  font: { fontSize: 35, color: 'white', opacity: 0.6 },
  title: { fontSize: 20, color: 'white', opacity: 0.6 },
});
