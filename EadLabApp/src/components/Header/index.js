import React from 'react';
import { Image, StyleSheet, Text, View, Platform, StatusBar } from 'react-native';
import eadLabLogo from '../../assets/ead_lab_logo.png';

export default function Header({ viewTitle }) {
  return (
    <View style={styles.container}>
      <Image style={styles.eadLabLogo} source={eadLabLogo} />
      <Text style={styles.viewTitle}>{viewTitle}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginVertical: 25,
  },
  eadLabLogo: {
    height: 55,
    width: 55,
    resizeMode: 'contain',
    margin: 10,
    opacity: 0.8,
  },

  viewTitle: {
    color: '#fff',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: '500',
  },
});
