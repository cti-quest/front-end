import React from 'react';
import { StyleSheet, Text, View, StatusBar, Platform, TouchableOpacity } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import { LinearGradient } from 'expo-linear-gradient';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { AntDesign } from '@expo/vector-icons';

import { IndexPath, Select, SelectItem } from '@ui-kitten/components';

export default function Ranking({ navigation }) {
  const headRank = (value) => <Text style={styles.headRank}>{value}</Text>;
  const headPlayer = (value) => <Text style={styles.headPlayer}>{value}</Text>;
  const headScore = (value) => <Text style={styles.headScore}>{value}</Text>;
  const headTime = (value) => <Text style={styles.headTime}>{value}</Text>;

  const colRank = (value) => <Text style={styles.colRank}>{value}</Text>;
  const colPlayer = (value) => <Text style={styles.colPlayer}>{value}</Text>;
  const colScore = (value) => <Text style={styles.colScore}>{value}</Text>;
  const colTime = (value) => <Text style={styles.colTime}>{value}</Text>;
  const renderToggleButton = () => <Button onPress={() => setVisible(true)}>TOGGLE MENU</Button>;

  const [selectedOption, setSelectedOption] = React.useState();
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const dataOptions = ['Jogo 1'];

  // const displayValue = dataOptions[selectedIndex.row];

  const renderOption = (title) => <SelectItem title={title} />;

  const [visible, setVisible] = React.useState(false);
  const tableHead = [
    headRank(' '),
    headPlayer('Participante'),
    headScore('Pontos'),
    headTime('Tempo'),
  ];
  const tableData = [
    [colRank('1º'), colPlayer('José G'), colScore('120'), colTime('01:20:21')],
    [colRank('2º'), colPlayer('João A'), colScore('100'), colTime('01:10:01')],
    [colRank('3º'), colPlayer('Luís C'), colScore('105'), colTime('01:00:21')],
    [colRank('4º'), colPlayer('André B'), colScore('105'), colTime('01:00:30')],
    [colRank('5º'), colPlayer('Pedro B'), colScore('100'), colTime('01:20:21')],
    [colRank('6º'), colPlayer('Maria S.'), colScore('92'), colTime('02:00:58')],
    [colRank('7º'), colPlayer('Katarina F.'), colScore('80'), colTime('01:20:21')],
    [colRank('8º'), colPlayer('Lucian F.'), colScore('60'), colTime('00:58:21')],
  ];

  const colWidth = [50, 110, 80, 60];
  const colHeight = [40, 40, 40, 40, 40, 40, 40, 40];

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.statusBarIOS} />
      <StatusBar backgroundColor={'#77bbd0'} />

      <View style={styles.container}>
        <Header viewTitle="Ranking" />
        {/* // seletor ---------------------------------- */}

        <View style={styles.cardContainer}>
          <View style={styles.buttonContainer}>
            <Select
              selectedIndex={selectedOption}
              value={'Jogo 1'}
              selectedIndex={selectedIndex}
              onSelect={(index) => setSelectedIndex(index)}
            >
              {dataOptions.map(renderOption)}
            </Select>

            {/* <TouchableOpacity
        style={styles.buttonContainer}
        onPress={onPress}>
        <Text>Press Here</Text>
      </TouchableOpacity> */}
          </View>

          <View style={styles.cardScroll}>
            <View style={styles.cardContainer}>
              <Table>
                <Row data={tableHead} style={styles.head} widthArr={colWidth} />
                <Rows data={tableData} widthArr={colWidth} heightArr={colHeight} />
              </Table>
            </View>
          </View>
        </View>
      </View>

      <Footer navigate={navigation.navigate} />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  statusBarIOS: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#77bbd0',
  },
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },

  head: {
    marginBottom: 5,
    opacity: 0.8,
  },
  texts: {
    marginVertical: 12,
  },

  headRank: {
    textAlign: 'left',
    color: '#DFE6EF',
    fontSize: 14,
    fontWeight: 'normal',
  },
  headPlayer: {
    textAlign: 'left',
    color: '#DFE6EF',
    fontSize: 14,
    fontWeight: 'normal',
  },
  headScore: {
    textAlign: 'center',
    color: '#DFE6EF',
    fontSize: 14,
    fontWeight: 'normal',
  },
  headTime: {
    textAlign: 'right',
    color: '#DFE6EF',
    fontSize: 14,
    fontWeight: 'normal',
  },

  colRank: {
    textAlign: 'left',
    color: '#345375',
    fontSize: 14,
    fontWeight: 'normal',
    opacity: 0.8,
  },
  colPlayer: {
    textAlign: 'left',
    color: '#DFE6EF',
    fontSize: 12,
    fontWeight: 'bold',
  },
  colScore: {
    textAlign: 'center',
    color: '#DFE6EF',
    fontSize: 12,
    fontWeight: 'bold',
  },
  colTime: {
    textAlign: 'right',
    color: '#DFE6EF',
    fontSize: 12,
    fontWeight: 'bold',
  },

  cardContainer: {
    // maxWidth: 420,
    // minWidth: 332,
  },
  cardScroll: {
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    minHeight: 90,
    maxHeight: 364,
    borderRadius: 5,
    padding: 15,

    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
  },

  textContainer: {
    paddingBottom: 20,
  },

  buttonContainer: {
    marginTop: 20,
    marginBottom: 26,
    backgroundColor: '#FFFF',
  },

  buttonCard: {
    height: 40,
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    padding: 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
  },

  buttonContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  buttonText: {
    color: '#DFE6EF',
    fontSize: 18,
    textAlign: 'left',
    fontWeight: 'normal',
  },
});
