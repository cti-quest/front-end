import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Platform,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Header from '../components/Header';
import Footer from '../components/Footer';

// só pra testar
const fakeList = [
  {
    id: '1',
    type: 'QUIZ:',
    title: 'POLÍTICA DA QUALIDADE.',
  },
  {
    id: '2',
    type: 'CAÇA PALAVRAS:',
    title: '[em breve]',
  },
  {
    id: '3',
    type: 'ASSOCIAÇÃO:',
    title: '[em breve]',
  },
  {
    id: '4',
    type: 'PREENCHER:',
    title: '[em breve]',
  },
  {
    id: '5',
    type: 'FORCA:',
    title: '[em breve]',
  },
];

const Item = ({ gameType, gameTitle, navigateToGame }) => (
  <View style={styles.cardContainer}>
    <TouchableOpacity activeOpacity={0.8} onPress={() => navigateToGame('Objetivos')}>
      <View style={styles.card}>
        <Text style={styles.texts}>{gameType}</Text>
        <Text style={styles.texts}>{gameTitle}</Text>
      </View>
    </TouchableOpacity>
  </View>
);

export default function Menu({ navigation }) {
  const renderItem = ({ item }) => (
    <Item gameType={item.type} gameTitle={item.title} navigateToGame={navigation.navigate} />
  );

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.statusBarIOS} />
      <StatusBar backgroundColor={'#77bbd0'} />

      <View style={styles.container}>
        <FlatList
          style={styles.listContainer}
          ListHeaderComponent={<Header viewTitle="Jogos" />}
          data={fakeList}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          ListFooterComponent={<View style={styles.listBottom} />}
        />
      </View>
      <Footer navigate={navigation.navigate} />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  statusBarIOS: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#77bbd0',
  },
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },

  listContainer: {
    marginBottom: 20,
  },
  listBottom: {
    height: 94,
  },
  cardContainer: {
    marginVertical: 10,
    marginHorizontal: 40,
  },
  card: {
    width: 420,
    height: 94,
    backgroundColor: '#252A45',
    padding: 15,
    paddingEnd: 100,
    borderRadius: 5,
    opacity: 0.55,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
  },

  texts: {
    color: '#fff',
    fontSize: 14,
    lineHeight: 22,
  },
});
