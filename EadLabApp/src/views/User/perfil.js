import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, StatusBar, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
// import { autocrop } from '../../../node_modules/jimp/types/index';

export default function Perfil({ navigation }) {
  const clicou = () => {
    Alert.alert('Logar');
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.statusBarIOS} />
      <StatusBar backgroundColor={'#77bbd0'} />
      <View style={styles.container}>
        <Header viewTitle="Perfil" />

        <View style={styles.mainContent}>
          <View style={styles.content}>
            <Text style={styles.title}>Logado como: </Text>
            <Text style={styles.text}>Usuário 1</Text>
          </View>

          <View style={styles.content}>
            <Text style={styles.title}>Email Logado:</Text>
            <Text style={styles.text}>user@user.com</Text>
          </View>
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Login')}>
            <View style={styles.buttonCard}>
              <Text style={styles.buttonText}>SAIR</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Footer navigate={navigation.navigate} />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  statusBarIOS: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#77bbd0',
  },
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },

  eadLabLogo: {
    // marginTop: 20,
    width: 110,
    height: 60,
    right: 0,
    top: 40,
    resizeMode: 'contain',
    margin: 1,
  },
  ead: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: -40,

    //fontVariant:2000,
    width: 200,
    right: 200,
    color: 'white',
    marginRight: -110,
  },

  login: {
    width: 100,
    height: 50,
    marginLeft: 15,
    // backgroundColor: '#0000ff',
    fontSize: 30,
    fontWeight: 'bold',
    marginVertical: 30,
    color: 'white',
  },

  mainContent: {
    width: 300,
    marginTop: 25,
  },
  content: {
    marginBottom: 24,
    textAlign: 'left',
  },

  title: {
    fontSize: 14,
    fontWeight: 'normal',
    color: 'white',
    marginBottom: 4,
  },

  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 4,
  },

  buttonContainer: {
    marginVertical: 3,
    marginLeft: 150,
  },

  buttonContainer: {
    marginVertical: 3,
  },

  buttonCard: {
    height: 36,
    backgroundColor: '#252A45',
    padding: 14,
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
    width: 300,
    height: 42,
    //backgroundColor: '#b2b2b2',
    marginTop: 10,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonText: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
  },
});
