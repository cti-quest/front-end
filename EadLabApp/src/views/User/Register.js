import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Platform,
  StatusBar,
  Alert,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import eadLabLogo from '../../assets/ead_lab_logo.png';
import Footer from '../../components/Footer';

export default function Register({ navigation }) {
  const clicou = () => {
    Alert.alert('Cadastrar');
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.statusBarIOS} />
      <StatusBar backgroundColor={'#77bbd0'} />

      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image style={styles.eadLabLogo} source={eadLabLogo} />
          <Text style={styles.viewTitle}>Cadastro</Text>
        </View>

        <View style={styles.formContainer}>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Nome:</Text>
            <TextInput
              style={styles.input}
              placeholder="ex: fulano"
              placeholderTextColor="#5784A6"
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>E-mail:</Text>
            <TextInput
              style={styles.input}
              placeholder="seu@email.com"
              placeholderTextColor="#5784A6"
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Senha:</Text>
            <TextInput style={styles.input} secureTextEntry={true} placeholder="" />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Confirme a senha:</Text>
            <TextInput style={styles.input} secureTextEntry={true} placeholder="" />
          </View>

          <View style={styles.buttonContainer}>
            <TouchableOpacity activeOpacity={0.8} onPress={() => clicou()}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Cadastrar-se</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <Text style={styles.link} onPress={() => navigation.navigate('Login')}>
          Já possui um acesso? Entre
        </Text>
        {/* </View> */}
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  logoContainer: {
    alignItems: 'center',
    marginVertical: 25,
  },
  eadLabLogo: {
    height: 38,
    width: 38,
    resizeMode: 'contain',
    margin: 10,
    opacity: 0.8,
  },
  viewTitle: {
    color: '#fff',
    fontSize: 34,
    textAlign: 'center',
    fontWeight: '500',
  },

  container: {
    flex: 1,
    alignItems: 'center',
  },

  formContainer: {
    width: 300,
    marginVertical: 10,
  },

  formGroup: {
    marginBottom: 25,
  },

  label: {
    marginBottom: 5,
    textAlign: 'left',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },

  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    paddingHorizontal: 16,
    height: 44,
    fontSize: 16,
    color: '#345375',
    borderRadius: 5,

    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
  },

  button: {
    backgroundColor: '#252A45',
    height: 54,
    padding: 15,
    marginVertical: 15,
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
  },

  buttonText: {
    color: '#fff',
    fontSize: 22,
    textAlign: 'center',
    fontWeight: '500',
  },

  link: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    opacity: 0.6,
  },
});
