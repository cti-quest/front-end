import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Platform,
  StatusBar,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import eadLabLogo from '../../assets/ead_lab_logo.png';

export default function Login({ navigation }) {
  const [message, setMessage] = useState('');

  const [userEmail, setUserEmail] = useState('');
  const [userPassword, setUserPassword] = useState('');

  const login = () => {
    navigation.navigate('Menu');
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.container}>
        <View style={styles.statusBarIOS} />
        <StatusBar backgroundColor={'#77bbd0'} />
        <Image style={styles.eadLabLogo} source={eadLabLogo} />

        <View style={styles.titleEad}>
          <Text style={styles.ead}>EAD-LAB</Text>
          <Text style={styles.treinamento}>TREINAMENTOS PERSONALIZADOS</Text>
        </View>

        <Text style={styles.login}>Login</Text>
        <Text>{message}</Text>
        <View style={styles.user}>
          <Text style={styles.userText}>Usuário:</Text>
          <TextInput
            onChangeText={(userEmail) => setUserEmail(userEmail)}
            value={userEmail}
            placeholder=" email@exemplo.com"
            placeholderTextColor="#000000"
            style={styles.textInput}
            underlineColorAndroid="transparent"
          />
        </View>

        <View style={styles.pass}>
          <Text style={styles.passText}>Senha:</Text>
          <TextInput
            onChangeText={(userPassword) => setUserPassword(userPassword)}
            defaultValue={userPassword}
            placeholder=" ********"
            secureTextEntry={true}
            style={styles.textInput}
            underlineColorAndroid="transparent"
            onChangeText={() => setUserEmail()}
          />
        </View>

        <Text style={styles.esqueci}>Esqueci a senha</Text>

        <View style={styles.buttonContainer}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => login()}>
            <View style={styles.buttonCard}>
              <Text style={styles.buttonText}>ENTRAR</Text>
            </View>
          </TouchableOpacity>
        </View>

        <Text style={styles.acesso} onPress={() => navigation.navigate('Cadastro')}>
          1º Acesso? Cadastre-se
        </Text>

        <Text style={styles.escrito_embaixo}>ead-lab.coursify.me</Text>
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  statusBarIOS: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#77bbd0',
  },
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 20,
  },

  eadLabLogo: {
    width: 130,
    height: 100,
    right: 115,
    resizeMode: 'contain',
    margin: 1,
    opacity: 0.8,
  },

  titleEad: {
    flexDirection: 'column',
    marginBottom: 26,
    top: -70,
  },
  ead: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  treinamento: {
    fontSize: 12,
    fontWeight: 'bold',
    marginRight: -100,
  },
  escrito_embaixo: {
    width: 200,
    height: 100,
    marginLeft: 90,
    marginTop: 80,
    //backgroundColor: '#0000ff',
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 10,
    color: 'white',
  },

  login: {
    width: 100,
    height: 50,
    marginTop: -40,
    //backgroundColor: '#0000ff',
    fontSize: 38,
    fontWeight: 'bold',
    marginVertical: 50,
    color: 'white',
  },
  textInput: {
    backgroundColor: '#ffffff',
    width: 300,
    opacity: 0.2,
    height: 38,
    padding: 10,
    color: '#252A45',
  },
  acesso: {
    width: 160,
    height: 20,
    marginRight: -20,
    color: 'white',
    //backgroundColor: '#0000ff',
    fontSize: 14,
    // fontWeight:"bold",
    marginVertical: 20,
  },
  esqueci: {
    width: 120,
    height: 20,
    marginTop: -6,
    marginRight: -190,
    color: 'white',

    //backgroundColor: '#0000ff',
    fontSize: 16,
    //fontWeight:"bold",
    marginVertical: 20,
  },
  user: {
    width: 105,
    height: 25,
    marginTop: -15,
    marginLeft: -200,
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 50,
    color: 'white',
  },
  userText: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 5,
  },

  pass: {
    width: 105,
    height: 20,
    marginTop: 15,
    marginLeft: -200,
    marginVertical: 50,
  },

  passText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },

  buttonContainer: {
    marginVertical: 3,
  },

  buttonCard: {
    backgroundColor: '#252A45',
    padding: 20,
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
    width: 300,
    height: 42,
    marginTop: 10,
    alignItems: 'center',
  },

  buttonText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '500',
  },
});
