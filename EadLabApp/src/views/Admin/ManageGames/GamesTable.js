import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { DataTable } from 'react-native-paper';
import { Feather } from '@expo/vector-icons';
import AdminHeader from '../../../components/Admin/Header';
import Title from '../../../components/Admin/Title';
import Button from '../../../components/Button';

import { getAllGames } from '../../../services/game';

const EditIcon = () => <Feather name="edit-3" size={18} color="gray" />;
const ViewIcon = () => <Feather name="eye" size={18} color="gray" />;
const DeleteIcon = () => <Feather name="trash-2" size={18} color="gray" />;

export default function GamesTable({ navigation }) {
  const [GAMES_LIST, SET_GAMES_LIST] = useState([]);

  useEffect(() => {
    getAllGames()
      .then(({ data }) => {
        SET_GAMES_LIST(data);
      })
      .catch((err) => console.log(err));
  }, []);

  const items = GAMES_LIST;

  const Pagination = () => {
    const [page, setPage] = useState(0);
    const itemsPerPage = 20;
    const from = page * itemsPerPage;
    const to = (page + 1) * itemsPerPage;

    return (
      <DataTable>
        <DataTable.Pagination
          page={page}
          numberOfPages={Math.floor(items.length / itemsPerPage)}
          onPageChange={(page) => setPage(page)}
          label={`${from + 1}-${to} of ${items.length}`}
        />
      </DataTable>
    );
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <ScrollView>
        <AdminHeader navigation={navigation} />
        <View style={styles.container}>
          <Title viewTitle="Tabela de Jogos" />
          <Button navigation={navigation} goTo="Registrar Jogo" text="Cadastrar jogo" />
          <DataTable style={styles.table}>
            <DataTable.Header style={styles.header}>
              <DataTable.Title style={styles.title}>ID</DataTable.Title>
              <DataTable.Title style={styles.title}>Título</DataTable.Title>
              <DataTable.Title style={styles.title}>Tipo</DataTable.Title>
              <DataTable.Title style={styles.title}>Objetivos</DataTable.Title>
              <DataTable.Title style={styles.title}>Opções</DataTable.Title>
            </DataTable.Header>

            {items.map((item) => {
              return (
                <DataTable.Row
                  key={item.id}
                  onPress={() => {
                    console.log(`selected item ${item.id}`);
                  }}
                >
                  <DataTable.Cell style={styles.cellContainer}>
                    <Text style={styles.scrollText}>{item.id}</Text>
                  </DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>
                    <Text style={styles.scrollText}>{item.title}</Text>
                  </DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>{item.type}</DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>
                    <Text numberOfLines={1} style={styles.text}>
                      {item.goals}
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>
                    <TouchableOpacity style={styles.options}>
                      <EditIcon />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.options}>
                      <ViewIcon />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.options}>
                      <DeleteIcon />
                    </TouchableOpacity>
                  </DataTable.Cell>
                </DataTable.Row>
              );
            })}

            <Pagination />
          </DataTable>
        </View>
      </ScrollView>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40,
    paddingHorizontal: 20,
  },

  table: {
    borderRadius: 5,
    backgroundColor: '#fff',
    maxWidth: 900,
  },
  header: {},
  title: {
    width: 50,
  },
  cellContainer: {
    flexDirection: 'row',
    maxWidth: 400,
  },

  text: {
    flex: 1,
  },

  options: {
    marginRight: 15,
  },
});
