import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { DataTable } from 'react-native-paper';
import { Feather } from '@expo/vector-icons';
import AdminHeader from '../../../../components/Admin/Header';
import Title from '../../../../components/Admin/Title';
import Button from '../../../../components/Button';

import { getAllQuestions } from '../../../../services/question';

const EditIcon = () => <Feather name="edit-3" size={18} color="gray" />;
const ViewIcon = () => <Feather name="eye" size={18} color="gray" />;
const DeleteIcon = () => <Feather name="trash-2" size={18} color="gray" />;

export default function ManageQuestions({ navigation }) {
  const [QUESTIONS_LIST, SET_QUESTIONS_LIST] = useState([]);

  useEffect(() => {
    getAllQuestions()
      .then(({ data }) => {
        SET_QUESTIONS_LIST(data);
      })
      .catch((err) => console.log(err));
  }, []);

  const items = QUESTIONS_LIST;

  const Pagination = () => {
    const [page, setPage] = useState(0);
    const itemsPerPage = 20;
    const from = page * itemsPerPage;
    const to = (page + 1) * itemsPerPage;

    return (
      <DataTable>
        <DataTable.Pagination
          page={page}
          numberOfPages={Math.floor(items.length / itemsPerPage)}
          onPageChange={(page) => setPage(page)}
          label={`${from + 1}-${to} of ${items.length}`}
        />
      </DataTable>
    );
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <ScrollView>
        <AdminHeader navigation={navigation} />
        <View style={styles.container}>
          <Title viewTitle="Tabela de Questões" />
          <Button navigation={navigation} goTo="Registrar Questão" text="Cadastrar Questão" />
          <DataTable style={styles.table}>
            <DataTable.Header style={styles.header}>
              <DataTable.Title style={styles.title}>ID</DataTable.Title>
              <DataTable.Title style={styles.title}>Questão</DataTable.Title>
              <DataTable.Title style={styles.title}>Alt. 1</DataTable.Title>
              <DataTable.Title style={styles.title}>Alt. 2</DataTable.Title>
              <DataTable.Title style={styles.title}>Alt. 3</DataTable.Title>
              <DataTable.Title style={styles.title}>Alt. 4</DataTable.Title>
              <DataTable.Title style={styles.title}>Resposta</DataTable.Title>
              <DataTable.Title style={styles.title}>Opções</DataTable.Title>
            </DataTable.Header>

            {items.map((item) => {
              return (
                <DataTable.Row
                  key={item.id}
                  onPress={() => {
                    console.log(`selected item ${item.id}`);
                  }}
                >
                  <DataTable.Cell style={styles.cellContainer}>
                    <Text style={styles.scrollText}>{item.id}</Text>
                  </DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>
                    <Text style={styles.scrollText}>{item.question}</Text>
                  </DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>{item.alternative_0}</DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>{item.alternative_1}</DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>{item.alternative_2}</DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>{item.alternative_3}</DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>{item.answer}</DataTable.Cell>
                  <DataTable.Cell style={styles.cellContainer}>
                    <TouchableOpacity style={styles.options}>
                      <EditIcon />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.options}>
                      <ViewIcon />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.options}>
                      <DeleteIcon />
                    </TouchableOpacity>
                  </DataTable.Cell>
                </DataTable.Row>
              );
            })}

            <Pagination />
          </DataTable>
        </View>
      </ScrollView>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40,
    paddingHorizontal: 20,
  },

  table: {
    borderRadius: 5,
    backgroundColor: '#fff',
    maxWidth: 1200,
  },
  header: {},
  title: {
    width: 50,
  },
  cellContainer: {
    flexDirection: 'row',
    maxWidth: 400,
  },

  text: {
    flex: 1,
  },

  options: {
    marginRight: 15,
  },
});
