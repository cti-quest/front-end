import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import AdminHeader from '../../../../components/Admin/Header';
import Title from '../../../../components/Admin/Title';
import { Radio } from '@ui-kitten/components';

export default function RegisterQuestion({ navigation }) {
  const [activeChecked, setActiveChecked] = React.useState(false);

  const clicou = () => {
    Alert.alert('Cadastrar');
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <ScrollView>
        <AdminHeader navigation={navigation} />
        <View style={styles.container}>
          <Title viewTitle="Registrar Questão - Jogo 1" />
          <View style={styles.formContainer}>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Questão:</Text>
              <TextInput style={styles.input} placeholderTextColor="#5784A6" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Alternativa 1:</Text>
              <TextInput style={styles.input} placeholderTextColor="#5784A6" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Alternativa 2:</Text>
              <TextInput style={styles.input} secureTextEntry={true} placeholder="" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Alternativa 3:</Text>
              <TextInput style={styles.input} secureTextEntry={true} placeholder="" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Alternativa 4:</Text>
              <TextInput style={styles.input} secureTextEntry={true} placeholder="" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Resposta correta:</Text>
              <View style={styles.radioContainer} level="1">
                <Radio
                  style={styles.radio}
                  checked={activeChecked}
                  onChange={(nextChecked) => setActiveChecked(nextChecked)}
                >
                  <Text style={styles.checkText}>Alternativa 1</Text>
                </Radio>

                <Radio style={styles.radio}>
                  <Text style={styles.checkText}>Alternativa 2</Text>
                </Radio>

                <Radio style={styles.radio}>
                  <Text style={styles.checkText}>Alternativa 3</Text>
                </Radio>

                <Radio style={styles.radio}>
                  <Text style={styles.checkText}>Alternativa 4</Text>
                </Radio>
              </View>
            </View>

            <View style={styles.buttonContainer}>
              <TouchableOpacity activeOpacity={0.8} onPress={() => clicou()}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Salvar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40,
    paddingHorizontal: 20,
  },

  formContainer: {
    maxWidth: 600,
    marginVertical: 20,
  },

  formGroup: {
    marginBottom: 25,
  },

  label: {
    marginBottom: 5,
    textAlign: 'left',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  labelCheckbox: {
    textAlign: 'left',
    color: 'white',
    fontSize: 16,
    fontWeight: 'normal',
  },

  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    paddingHorizontal: 16,
    height: 44,
    fontSize: 16,
    color: '#345375',
    borderRadius: 5,

    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
  },

  button: {
    backgroundColor: '#252A45',
    height: 40,
    padding: 15,
    marginVertical: 15,
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
  },

  buttonText: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
  },

  link: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    opacity: 0.6,
  },

  radioContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  radio: {
    margin: 2,
    paddingTop: 5,
    paddingBottom: 5,
  },
  checkText: {
    marginBottom: 5,
    textAlign: 'left',
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
});
