import React from 'react';
import { View, StyleSheet } from 'react-native';
import AdminHeader from '../../../../components/Admin/Header';
import { LinearGradient } from 'expo-linear-gradient';

export default function EditQuestion({ navigation }) {
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <AdminHeader navigation={navigation}></AdminHeader>
      <View style={styles.container}></View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
});
