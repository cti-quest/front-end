import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import AdminHeader from '../../../components/Admin/Header';
import Title from '../../../components/Admin/Title';
import { CheckBox } from '@ui-kitten/components';

export default function RegisterGame({ navigation }) {
  const [checked, setChecked] = React.useState(false);
  const clicou = () => {
    Alert.alert('Cadastrar');
  };

  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <ScrollView>
        <AdminHeader navigation={navigation} />
        <View style={styles.container}>
          <Title viewTitle="Registrar Jogo" />
          <View style={styles.formContainer}>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Tipo:</Text>
              <TextInput style={styles.input} placeholderTextColor="#5784A6" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Título:</Text>
              <TextInput style={styles.input} placeholderTextColor="#5784A6" />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Objetivos:</Text>
              <TextInput
                style={styles.inputMultiline}
                secureTextEntry={true}
                placeholder=""
                maxLength={400}
                numberOfLines={6}
                multiline
              />
            </View>

            <View style={styles.formGroup}>
              <CheckBox
                checked={checked}
                onChange={(nextChecked) => setChecked(nextChecked)}
                style={styles.checkbox}
              >
                {<Text style={styles.labelCheckbox}>Em desenvolvimento</Text>}
              </CheckBox>
            </View>

            <View style={styles.buttonContainer}>
              <TouchableOpacity activeOpacity={0.8} onPress={() => clicou()}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Salvar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40,
    paddingHorizontal: 20,
  },

  formContainer: {
    marginVertical: 20,
    width: 500,
  },

  formGroup: {
    marginBottom: 25,
  },

  label: {
    marginBottom: 5,
    textAlign: 'left',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  labelCheckbox: {
    textAlign: 'left',
    color: 'white',
    fontSize: 16,
    fontWeight: 'normal',
  },

  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    paddingHorizontal: 16,
    height: 44,
    fontSize: 16,
    color: '#345375',
    borderRadius: 5,
    width: '100%',

    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
  },
  inputMultiline: {
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    padding: 16,
    fontSize: 16,
    color: '#345375',
    borderRadius: 5,
    width: '100%',

    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
  },

  button: {
    backgroundColor: '#252A45',
    height: 40,
    padding: 15,
    marginVertical: 15,
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
  },

  buttonText: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
  },

  link: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    opacity: 0.6,
  },
});
