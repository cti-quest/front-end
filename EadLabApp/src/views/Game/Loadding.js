import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Platform,
  StatusBar,
  ProgressBarAndroid,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function Loading() {
  state = { position: 0.0 };
  progress = () => {
    this.setState;
    {
      this.state.position + 0.1;
    }
  };
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.container}>
        <ProgressBarAndroid
          styleAttr="Horizontal"
          inderterminate={false}
          progress={this.state.position}
          style={styles.ProgressBarAndroid}
        ></ProgressBarAndroid>
      </View>
    </LinearGradient>
  );
}
const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: Platform.OS ? StatusBar.currentHeight : 0,
  },

  ProgressBarAndroid: {
    flex: 1,
    justifyContent: 'space-evenly',
    padding: 10,
  },
});
