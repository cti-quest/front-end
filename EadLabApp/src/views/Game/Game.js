import React, { useEffect, useState, useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Card from '../../components/QuizCard';
import Timer from '../../components/Timer';
import { getAllQuestions } from '../../services/question';
import Load from '../../components/Load';
import Score from '../../components/Score';
import { TimerContext } from '../../contexts/timer';
import { GameContext } from '../../contexts/game';
import Status from '../../components/Status';

export default function Menu({ navigation }) {
  const { askIndex, questionsForAsk, setQuestions } = useContext(GameContext);
  const [isLoading, setIsLoading] = useState(true);
  const { setPause } = useContext(TimerContext);

  useEffect(() => {
    getAllQuestions()
      .then(({ data }) => {
        setQuestions(data);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    if (questionsForAsk) {
      setIsLoading(false);
      setPause(false);
    }
  }, [questionsForAsk]);

  if (isLoading) return <Load />;
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      {askIndex !== 5 ? (
        <View style={styles.Container}>
          <View style={styles.Header}>
            <Timer />
            <Score />
          </View>
          <Card data={askIndex < 5 && questionsForAsk[askIndex]} index={askIndex} />
        </View>
      ) : (
        // <Status />
        navigation.navigate('Ranking')
      )}
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  Container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  Header: {
    width: '100%',
    maxWidth: 300,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
