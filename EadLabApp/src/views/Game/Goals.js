import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Header from '../../components/Header';

const textExample = `Responda a questão escolhendo uma das quatro alternativas

-> Cada resposta correta é equivalente a pontos positivos +

-> Cada resposta incorreta é equivalente a pontos negativos -

O contador de tempo será iniciado assim que apertar o botão JOGAR

Mas não se preocupe! Não existe um tempo para terminar.

O tempo serve para somente calcular o ranking!

Bom jogo!
`;

export default function Goals({ navigation }) {
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.statusBarIOS} />
      <StatusBar backgroundColor={'#77bbd0'} />

      <View style={styles.container}>
        <Header viewTitle="Objetivos" />

        <View style={styles.cardContainer}>
          <ScrollView style={styles.cardScroll}>
            <Text style={styles.textContainer}>
              <Text style={styles.texts}>{textExample}</Text>
            </Text>
          </ScrollView>

          <View style={styles.buttonContainer}>
            <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Jogo')}>
              <View style={styles.buttonCard}>
                <Text style={styles.buttonText}>JOGAR</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  statusBarIOS: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#77bbd0',
  },
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },

  cardContainer: {
    maxWidth: 420,
    minWidth: 332,
    marginVertical: 10,
    marginHorizontal: 40,
  },
  cardScroll: {
    backgroundColor: '#252A45',
    minHeight: 90,
    maxHeight: 344,
    borderRadius: 5,
    opacity: 0.55,
    padding: 15,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,
    elevation: 8,
  },

  textContainer: {
    paddingBottom: 20,
  },

  texts: {
    color: '#fff',
    fontSize: 14,
  },

  buttonContainer: {
    marginVertical: 40,
  },

  buttonCard: {
    height: 64,
    backgroundColor: '#252A45',
    padding: 15,
    borderRadius: 5,
    opacity: 0.55,
    justifyContent: 'center',
    alignContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.85,
    shadowRadius: 5,

    elevation: 8,
  },

  buttonText: {
    color: '#fff',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: '500',
  },
});
