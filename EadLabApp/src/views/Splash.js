import React from 'react';
import { Image, StyleSheet, View, ActivityIndicator } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import eadLabLogo from '../assets/ead_lab_logo.png';

export default function Splash() {
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.containerLogo}>
        <Image style={styles.eadLabLogo} source={eadLabLogo} />
      </View>
      <View style={styles.containerLoad}>
        <ActivityIndicator size="large" color="white" />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  containerLogo: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  containerLoad: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  eadLabLogo: {
    opacity: 0.7,
    width: '50%',
    resizeMode: 'contain',
    margin: 10,
  },
});
