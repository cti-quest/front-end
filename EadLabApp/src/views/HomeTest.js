import React from 'react';
import { Image, StyleSheet, Text, View, Platform, StatusBar } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Footer from '../components/Footer';
import Card from '../components/QuizCard';

export default function HomeTest({ navigation }) {
  console.log(navigation.navigate);
  return (
    <LinearGradient colors={['#77bbd0', '#31659e']} style={styles.LinearGradient}>
      <View style={styles.container}>
        <Card
          number={'01'}
          question={'Aqui vai a pergunta do Orlando ?'}
          responseA={'Aqui vai a alternativa A'}
          responseB={'Aqui vai a alternativa B'}
          responseC={'Aqui vai a alternativa C'}
          responseD={'Aqui vai a alternativa D'}
        />
        <Footer navigate={navigation.navigate} />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS ? StatusBar.currentHeight : 0,
  },
  eadLabLogo: {
    height: 55,
    width: 55,
    resizeMode: 'contain',
    margin: 10,
    opacity: 0.7,
  },
});
