import React from 'react';
import { StyleSheet, View, StatusBar, Platform } from 'react-native';
import {
  IndexPath,
  Select,
  SelectItem,
  Text,
  BottomNavigation,
  BottomNavigationTab,
  MenuItem,
  OverflowMenu,
  Button,
} from '@ui-kitten/components';
import { LinearGradient } from 'expo-linear-gradient';
import * as colors from '../config/colors';

const data = [
  { title: 'Menu Item 1' },
  { title: 'Menu Item 2' },
  { title: 'Menu Item 3' },
  { title: 'Menu Item 4' },
];

export default function KittenTest() {
  const [selectedOption, setSelectedOption] = React.useState(new IndexPath(0));
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const [visible, setVisible] = React.useState(false);
  const [selectedMenuItem, setSelectedMenuItem] = React.useState(null);
  const onItemSelect = (index) => {
    setSelectedMenuItem(index);
    setVisible(false);
  };

  const renderToggleButton = () => <Button onPress={() => setVisible(true)}>TOGGLE MENU</Button>;

  return (
    <LinearGradient colors={[colors.lightBlue, colors.darkBlue]} style={{ flex: 1 }}>
      <View style={styles.statusBarIOS} />
      <StatusBar backgroundColor={'#77bbd0'} />

      <View style={styles.container}>
        <Text>Welcome to UI Kitten</Text>
        <Select selectedIndex={selectedOption} onSelect={(index) => setSelectedOption(index)}>
          <SelectItem title="Option 1" />
          <SelectItem title="Option 2" />
          <SelectItem title="Option 3" />
        </Select>

        <OverflowMenu
          anchor={renderToggleButton}
          backdropStyle={styles.backdrop}
          visible={visible}
          selectedIndex={selectedMenuItem}
          onSelect={onItemSelect}
          onBackdropPress={() => setVisible(false)}
        >
          <MenuItem title="Users" />
          <MenuItem title="Orders" />
          <MenuItem title="Transactions" />
        </OverflowMenu>
      </View>

      <BottomNavigation
        selectedIndex={selectedIndex}
        onSelect={(index) => setSelectedIndex(index)}
        appearance="noIndicator"
      >
        <BottomNavigationTab title="USERS" />
        <BottomNavigationTab title="ORDERS" />
        <BottomNavigationTab title="TRANSACTIONS" />
      </BottomNavigation>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  statusBarIOS: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#77bbd0',
  },
  container: {
    flex: 1,
  },

  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
