import React, { createContext, useEffect, useState } from 'react';

const TimerContext = createContext();

const TimerProvider = ({ children }) => {
  const [time, setTime] = useState({ minutes: 0, seconds: 0, thousandth: 0 });
  const [pause, setPause] = useState(true);
  function resetTime() {
    setTime((time) => {
      return {
        thousandth: 0,
        seconds: 0,
        minutes: 0,
      };
    });
  }
  setTimeout(() => {
    if (!pause) {
      setTime((time) => {
        return {
          thousandth: time.thousandth === 59 ? 0 : time.thousandth + 1,
          seconds:
            time.seconds === 59 ? 0 : time.thousandth === 59 ? time.seconds + 1 : time.seconds,
          minutes: time.seconds === 59 ? time.minutes + 1 : time.minutes,
        };
      });
    }
  }, 20);

  return (
    <TimerContext.Provider value={{ time, setPause, resetTime }}>{children}</TimerContext.Provider>
  );
};

export { TimerContext, TimerProvider };
