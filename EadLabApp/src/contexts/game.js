import React, { createContext, useEffect, useState } from 'react';

const GameContext = createContext();

const GameProvider = ({ children }) => {
  const [questions, setQuestions] = useState(null);
  const [questionsForAsk, setQuestionsForAsk] = useState(null);
  const [askIndex, setAskIndex] = useState(0);
  function getRandom(max) {
    return Math.floor(Math.random() * max);
  }
  useEffect(() => {
    if (questions) {
      let forAsk = [];

      while (forAsk.length < 5) {
        let ask = questions[getRandom(questions.length)];
        let save = true;
        forAsk.map((quest) => {
          if (quest.id === ask.id) save = false;
        });
        if (save) forAsk.push(ask);
      }
      setQuestionsForAsk(forAsk);
    }
  }, [questions]);

  // useEffect(() => {
  //   if (askIndex === 6) {
  //     setAskIndex(0);
  //   }
  // }, [askIndex]);
  return (
    <GameContext.Provider
      value={{
        questions,
        setQuestions,
        questionsForAsk,
        setQuestionsForAsk,
        askIndex,
        setAskIndex,
      }}
    >
      {children}
    </GameContext.Provider>
  );
};

export { GameContext, GameProvider };
