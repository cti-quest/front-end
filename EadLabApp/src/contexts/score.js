import React, { createContext, useState } from 'react';

const ScoreContext = createContext();

const ScoreProvider = ({ children }) => {
  const [score, setScore] = useState(0);
  const resetScore = () => {
    setScore(0);
  };

  const addPoints = () => {
    setScore(score + 10);
  };

  return (
    <ScoreContext.Provider value={{ score, addPoints, resetScore }}>
      {children}
    </ScoreContext.Provider>
  );
};

export { ScoreProvider, ScoreContext };
