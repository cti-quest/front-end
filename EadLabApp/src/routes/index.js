import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

// **************** Admin Views ****************
// ===== Manage GAMES =====
import GamesTable from '../views/Admin/ManageGames/GamesTable';
import RegisterGame from '../views/Admin/ManageGames/RegisterGame';
// import EditGame from '../views/Admin/ManageGames/EditGame';
import RankingTable from '../views/Admin/ManageGames/RankingTable';

// ---> Game 1 - Quizz - Poítica da Qualidade
import QuestionsTable from '../views/Admin/ManageGames/Game1/QuestionsTable';
import RegisterQuestion from '../views/Admin/ManageGames/Game1/RegisterQuestion';
// import EditQuestion from '../views/Admin/ManageGames/Game1/EditQuestion';

// ===== Manage USERS =====
import UsersTable from '../views/Admin/ManageUsers/UsersTable';

// **************** User Views ****************
import Login from '../views/User/Login';
import Register from '../views/User/Register';
import Profile from '../views/User/perfil';

// **************** App Views ****************
// import Splash from '../views/Splash';
import Menu from '../views/Menu';
import Ranking from '../views/Ranking';

// **************** Game Views ****************
import Loading from '../views/Game/Loadding';
import Goals from '../views/Game/Goals';
import Game from '../views/Game/Game';

// Test Views ****************
// import HomeTest from '../views/HomeTest';

import CustomDrawerContent from '../components/Admin/CustomDrawerContent';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function AdminViews() {
  return (
    <Drawer.Navigator
      initialRouteName="Games Table"
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      {/* =================== MANAGE GAMES =================== */}
      <Drawer.Screen name="Tabela de Jogos" component={GamesTable} />
      <Drawer.Screen name="Registrar Jogo" component={RegisterGame} />
      {/* <Drawer.Screen name="Editar Jogo" component={EditGame} /> */}
      <Drawer.Screen name="Tabela de Rankings" component={RankingTable} />

      {/* ----> Game 1 - Quizz - Poítica da Qualidade */}
      <Drawer.Screen name="Tabela de Questões" component={QuestionsTable} />
      <Drawer.Screen name="Registrar Questão" component={RegisterQuestion} />
      {/* <Drawer.Screen name="Editar Questão" component={EditQuestion} /> */}

      {/* =================== MANAGE USERS =================== */}
      <Drawer.Screen name="Tabela de Usuários" component={UsersTable} />
    </Drawer.Navigator>
  );
}

function UserViews() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <Stack.Screen name="Cadastro" component={Register} options={{ headerShown: false }} />
      {/* <Stack.Screen name="Home Test" component={HomeTest} options={{ headerShown: false }} /> */}

      {/* ************************** APP VIEWS ************************** */}
      {/* <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} /> */}
      <Stack.Screen name="Menu" component={Menu} options={{ headerShown: false }} />
      <Stack.Screen name="Ranking" component={Ranking} options={{ headerShown: false }} />
      <Stack.Screen name="Perfil" component={Profile} options={{ headerShown: false }} />

      {/* ************************** GAME VIEWS ************************** */}
      <Stack.Screen name="Loading" component={Loading} options={{ headerShown: false }} />
      <Stack.Screen name="Objetivos" component={Goals} options={{ headerShown: false }} />
      <Stack.Screen name="Jogo" component={Game} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}

function NotLoggedViews() {
  return (
    <Stack.Navigator>
      {/* ************************** NOT SIGNED VIEWS ************************** */}
      <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <Stack.Screen name="Cadastro" component={Register} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}
export default function Routes() {
  // const [isLoading, setIsLoading] = useState(false);
  // const [isSignout, setIsSignout] = useState(false);
  // const [userToken, setUserToken] = useState(null);
  const [isSignedIn, setIsSignedIn] = useState(true);
  const [isAdmin, setIsAdmin] = useState(false);

  return (
    <NavigationContainer>
      {isSignedIn ? isAdmin ? <AdminViews /> : <UserViews /> : <NotLoggedViews />}
    </NavigationContainer>
  );
}
