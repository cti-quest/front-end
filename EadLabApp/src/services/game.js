import api from './index';

const getAllGames = async () => {
  const { data } = await api.get('/games');
  // console.log(data);
  return data;
};

const getGame = async (gameID) => {
  const { data } = await api.get(`/games/${gameID}`);
  console.log(data);
  return data;
};

const createGame = async (game) => {
  const { data } = await api.post('/games', game);
  console.log(data);
  return data;
};

const updateGame = async (gameID, game) => {
  const { data } = await api.post(`/games/${gameID}`, game);
  console.log(data);
  return data;
};

const deleteGame = async (gameID) => {
  const { data } = await api.delete(`/games/${gameID}`);
  console.log(data);
  return data;
};

export { getAllGames, getGame, createGame, updateGame, deleteGame };
