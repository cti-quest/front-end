import api from './index';

const getAllUsers = async () => {
  const { data } = await api.get('/users');
  console.log(data);
  return data;
};

const getUser = async (userID) => {
  const { data } = await api.get(`/users/${userID}`);
  console.log(data);
  return data;
};

const createUser = async (user) => {
  const { data } = await api.post('/users', user);
  console.log(data);
  return data;
};

const updateUser = async (userID, user) => {
  const { data } = await api.post(`/users/${userID}`, user);
  console.log(data);
  return data;
};

const deleteUser = async (userID) => {
  const { data } = await api.delete(`/users/${userID}`);
  console.log(data);
  return data;
};

export { getAllUsers, getUser, createUser, updateUser, deleteUser };
