import api from './index';

const getAllQuestions = async () => {
  const { data } = await api.get('/questions');
  console.log(data);
  return data;
};

const getQuestion = async (questionID) => {
  const { data } = await api.get(`/questions/${questionID}`);
  console.log(data);
  return data;
};

const createQuestion = async (question) => {
  const { data } = await api.post('/questions', question);
  console.log(data);
  return data;
};

const updateQuestion = async (questionID, question) => {
  const { data } = await api.post(`/questions/${questionID}`, question);
  console.log(data);
  return data;
};

const deleteQuestion = async (questionID) => {
  const { data } = await api.delete(`/questions/${questionID}`);
  console.log(data);
  return data;
};

export { getAllQuestions, getQuestion, createQuestion, updateQuestion, deleteQuestion };
