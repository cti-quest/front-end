import axios from 'axios';

// local: http://localhost:8000
// heroku: https://vci-quest-api.herokuapp.com

const api = axios.create({
  baseURL: 'https://vci-quest-api.herokuapp.com',
});

export default api;
