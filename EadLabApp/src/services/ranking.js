import api from './index';

const getRankingByGame = async () => {
  const { data } = await api.get(`/ranking`);
  console.log(data);
  return data;
};

const getScore = async (rankingID) => {
  const { data } = await api.get(`/ranking/${rankingID}`);
  console.log(data);
  return data;
};

const createScore = async (ranking) => {
  const { data } = await api.post('/ranking', ranking);
  console.log(data);
  return data;
};

const updateScore = async (rankingID, ranking) => {
  const { data } = await api.post(`/ranking/${rankingID}`, ranking);
  console.log(data);
  return data;
};

const deleteScore = async (rankingID) => {
  const { data } = await api.delete(`/ranking/${rankingID}`);
  console.log(data);
  return data;
};

export { getRankingByGame, getScore, createScore, updateScore, deleteScore };
