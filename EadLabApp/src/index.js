import React from 'react';
import Routes from './routes';
import { TimerProvider } from './contexts/timer';
import { ScoreProvider } from './contexts/score';
import { GameProvider } from './contexts/game';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';
import { Provider as PaperProvider } from 'react-native-paper';

export default function App() {
  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      <PaperProvider>
        <TimerProvider>
          <ScoreProvider>
            <GameProvider>
              <Routes />
            </GameProvider>
          </ScoreProvider>
        </TimerProvider>
      </PaperProvider>
    </ApplicationProvider>
  );
}
