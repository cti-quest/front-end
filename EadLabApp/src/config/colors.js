// const colors = {
//   lightBlue: '#77bbd0',
//   darkBlue: '#31659e',
//   background: [lightBlue, darkBlue],

//   lightText: '#fff',
//   darkText: '#2c2c2c',
//   inputText: '345375',

//   card: '#252a45',
//   option: '#181e34',

//   bottomNav: '#4b7aab',
// };

const lightBlue = '#77bbd0';
const darkBlue = '#31659e';

export { lightBlue, darkBlue };
