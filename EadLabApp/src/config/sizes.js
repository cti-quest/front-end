export default {
  logo: 95,
  headerLogo: 55,

  eadLab: 16,
  treinamentos: 12,

  // the giants titles
  title_32: 32,

  // button, headerTitle
  title_22: 22,

  // label, input
  text_18: 18,

  // all texts, minilinks
  text_14: 14,

  // progressGameBar
  text_12: 12,
};
